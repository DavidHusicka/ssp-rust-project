use std::error;

/// Adds two numbers together.
/// 
/// # Examples
/// 
/// ```
/// let result = add(1.0, 2.0);
/// assert_eq!(result, 3.0);
/// ```
fn add(a: f64, b: f64) -> f64 {
    a + b
}

#[test]
fn test_add_positive() {
    assert_eq!(add(1.0, 2.0), 3.0);
}

#[test]
fn test_add_negative() {
    assert_eq!(add(-1.0, -2.0), -3.0);
}

#[test]
fn test_add_zero() {
    assert_eq!(add(1.0, 0.0), 1.0);
}

#[test]
fn test_add_float() {
    assert_eq!(add(1.0, 0.5), 1.5);
}

#[test]
fn test_add_float_negative() {
    assert_eq!(add(-1.0, -0.5), -1.5);
}

#[test]
fn test_add_nan() {
    assert!(add(1.0, std::f64::NAN).is_nan());
}

/// Subtracts two numbers.
/// 
/// # Examples
/// 
/// ```
/// let result = sub(1.0, 2.0);
/// assert_eq!(result, -1.0);
/// ```
fn sub(a: f64, b: f64) -> f64 {
    a - b
}

#[test]
fn test_sub_positive() {
    assert_eq!(sub(1.0, 2.0), -1.0);
}

#[test]
fn test_sub_negative() {
    assert_eq!(sub(-1.0, -2.0), 1.0);
}

#[test]
fn test_sub_zero() {
    assert_eq!(sub(1.0, 0.0), 1.0);
}

#[test]
fn test_sub_float() {
    assert_eq!(sub(1.0, 0.5), 0.5);
}

#[test]
fn test_sub_float_negative() {
    assert_eq!(sub(-1.0, -0.5), -0.5);
}

#[test]
fn test_sub_nan() {
    assert!(sub(1.0, std::f64::NAN).is_nan());
}

/// Multiplies two numbers.
/// 
/// # Examples
/// 
/// ```
/// let result = mul(1.0, 2.0);
/// assert_eq!(result, 2.0);
/// ```
fn mul(a: f64, b: f64) -> f64 {
    a * b
}

#[test]
fn test_mul_positive() {
    assert_eq!(mul(1.0, 2.0), 2.0);
}

#[test]
fn test_mul_negative() {
    assert_eq!(mul(-1.0, -2.0), 2.0);
}

#[test]
fn test_mul_zero() {
    assert_eq!(mul(1.0, 0.0), 0.0);
}

#[test]
fn test_mul_float() {
    assert_eq!(mul(1.0, 0.5), 0.5);
}

#[test]
fn test_mul_nan() {
    assert!(mul(1.0, std::f64::NAN).is_nan());
}

/// Divides two numbers.
/// 
/// # Examples
/// 
/// ```
/// let result = div(1.0, 2.0);
/// assert_eq!(result, 0.5);
/// ```
fn div(a: f64, b: f64) -> f64 {
    a / b
}

#[test]
fn test_div_positive() {
    assert_eq!(div(1.0, 2.0), 0.5);
}

#[test]
fn test_div_negative() {
    assert_eq!(div(-1.0, -2.0), 0.5);
}

#[test]
fn test_div_zero() {
    assert_eq!(div(1.0, 0.0), std::f64::INFINITY);
}

#[test]
fn test_div_float() {
    assert_eq!(div(1.0, 0.5), 2.0);
}

#[test]
fn test_div_nan() {
    assert!(div(1.0, std::f64::NAN).is_nan());
}


/// Gets input from the user.
/// 
/// # Examples
/// 
/// ```
/// let (a, b, string) = get_input().unwrap(); // string is an operator
/// ```
fn get_input() -> Result<(f64, f64, String), Box<dyn error::Error>> {
    println!("Enter a expression: ");
    let mut input = String::new();
    std::io::stdin().read_line(&mut input)?;
    let mut input = input.split_whitespace();
    let Some(a_string) = input.next() else {
        return Err("No input".into());
    };
    let a = a_string.parse::<f64>()?;
    let string = input.next().unwrap().to_string();
    let Some(b_string) = input.next() else {
        return Err("No input".into());
    };
    let b = b_string.parse::<f64>()?;
    Ok((a, b, string))
}

/// Operates on two numbers.
/// 
/// # Examples
/// 
/// ```
/// let result = operate(1.0, 2.0, "+".to_string());
/// assert_eq!(result, Ok(3.0));
/// ```
fn operate(a: f64, b: f64, string: String) -> Result<f64, ()> {
    match string.as_str() {
        "+" => Ok(add(a, b)),
        "-" => Ok(sub(a, b)),
        "*" => Ok(mul(a, b)),
        "/" => Ok(div(a, b)),
        _ => Err(()),
    }
}

#[test]
fn test_operate_add() {
    assert_eq!(operate(1.0, 2.0, "+".to_string()), Ok(3.0));
}

#[test]
fn test_operate_sub() {
    assert_eq!(operate(1.0, 2.0, "-".to_string()), Ok(-1.0));
}

#[test]
fn test_operate_mul() {
    assert_eq!(operate(1.0, 2.0, "*".to_string()), Ok(2.0));
}

#[test]
fn test_operate_div() {
    assert_eq!(operate(1.0, 2.0, "/".to_string()), Ok(0.5));
}

#[test]
fn test_operate_invalid() {
    assert_eq!(operate(1.0, 2.0, "a".to_string()), Err(()));
}



fn main() {
    loop {
        let Ok(input) = get_input() else {
            println!("Invalid input");
            continue;
        };
        let (a, b, string) = input;
        let Ok(result) = operate(a, b, string) else {
            println!("Invalid input");
            continue;
        };
        println!("Result: {}", result);
    }
}
