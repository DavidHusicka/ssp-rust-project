# SSP Big Rust Project

This is a big project for the subject SSP (Servicing of Software Projects). It has an example Rust project that is being built. It is a simple calculator that can add, subtract, multiply and divide. You can find a Linux binary in build artifacts or build and run it yourself with `cargo run`.
